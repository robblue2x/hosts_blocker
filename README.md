# hosts blocker

Linux only

captures DNS requests from your machine appends entries to your hosts file to block malicious domains automatically blocks domain suffixes

## install

try:

```
sudo apt-get install python3-scapy
```

if that install fails, install scapy with pip

```
sudo apt-get install python3-pip
pip3 install scapy-python3
```

update and install blacklist

```
cd hosts_blocker
git pull
./hosts_blocker.py update
```

## uninstall

```
sudo mv /etc/hosts.orig /etc/hosts
```

## run capture

```
cd hosts_blocker
./cap
```

you'll see output like (visiting amazon.co.uk):

```
# ./cap
loaded 24 autoblock entries
./block 'amazon.co.uk.'
./block 'www.amazon.co.uk.'
./block 'images-na.ssl-images-amazon.com.'
./block 'images-eu.ssl-images-amazon.com.'
./block 'fls-eu.amazon.com.'
./block 'dq4ijymydgrfx.cloudfront.net.'
./block 'aax-eu.amazon-adsystem.com.'
```

## block some hosts

```
cd hosts_blocker
./block imageads.googleadservices.com
./block imageads1.googleadservices.com
```

## autoblock domains

when `./cap` is running if it finds a hostname that ends with one of the
 suffixes in the `autoblock` file it will automatically add it to the blacklist
 . too add a malicious domain suffix simple append it to the `autoblock` file:
 (or edit the file with your favourite editor)
```
echo ".adsrvr.org" >> autoblock
```
