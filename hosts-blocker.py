#!/usr/bin/python3

import re
import sys
import os
from shutil import copyfile
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import scapy.all
import re

HOSTS = "/etc/hosts"
ORIGINAL = "/etc/hosts.orig"
BLACKLIST = "blacklist"
AUTOBLOCK = "autoblock"


def load_blacklist():
    if not os.path.isfile(BLACKLIST):
        return []
    blacklist = []
    with open(BLACKLIST, "r") as f:
        blacklist = f.readlines()
    return blacklist


def save_blacklist(blacklist):
    blacklist.sort()
    last = ""
    with open(BLACKLIST, "w") as f:
        for l in blacklist:
            l = l.strip()
            l = l.replace("\t", " ")
            l = l + "\n"
            if last != l:
                f.write(l)
            last = l


def load_autoblock():
    if not os.path.isfile(AUTOBLOCK):
        return []
    autoblock = []
    with open(AUTOBLOCK, "r") as f:
        for line in f.readlines():
            if len(line) > 3:
                autoblock.append(line.strip())
    return autoblock


def load_original():
    if not os.path.isfile(ORIGINAL):
        return []
    original = []
    with open(ORIGINAL, "r") as f:
        original = f.readlines()

    return original


def save_hosts(original, blacklist):
    if not os.path.isfile(ORIGINAL):
        copyfile(HOSTS, ORIGINAL)
    hosts = open(HOSTS, "w")
    for l in original:
        hosts.write(l)
    hosts.write("\n# start of blacklist\n")
    for l in blacklist:
        hosts.write(l)
    hosts.close()


def update():
    print("update")
    save_hosts(load_original(), load_blacklist())


def add(hostname):
    blacklist = load_blacklist()
    blacklist.append("0.0.0.0 " + hostname)
    save_blacklist(blacklist)
    update()


def remove():
    remove = "0.0.0.0 " + sys.argv[2]
    print("remove", remove)
    blacklist = load_blacklist()
    with open(BLACKLIST, "w") as f:
        for l in blacklist:
            if l.strip() != remove.strip():
                f.write(l)
    update()


capture_list = []
autoblock = []


def cb(pkt):
    prefix = sys.argv[2]
    r = re.compile("(\".*\")")
    if scapy.all.DNSQR in pkt and pkt.dport == 53:
        s = pkt.summary()
        d = r.search(s).groups()[0].strip('\'".')
        if d not in capture_list:
            for a in autoblock:
                if d.endswith(a):
                    print("autoblocking " + d)
                    add(d)
                    capture_list.append(d)
                    return
            capture_list.append(d)

            d = prefix + d
            for warn in ["track", "advert", "analytics", "click", "metric"]:
                if warn in d:
                    d = d + " ### !WARN!"
            print(d)


def capture():
    global autoblock
    autoblock = load_autoblock()
    print("loaded " + str(len(autoblock)) + " autoblock entries")
    f = 'udp and port 53'
    scapy.all.sniff(filter=f, store=0,  prn=cb)


def printHelp():
    print("Usage:")
    print("sudo ./hosts_blocker action [arg]")
    print("actions:")
    print("update            - update the hosts file at /etc/hosts")
    print("add <hostname>    - add <hostname> to the blocklist")
    print("remove <hostname> - remove <hostname> from the blocklist")
    print("capture <prefix>  - capture dns requests from this system")
    print("help              - print this help text")


def main():
    if len(sys.argv) == 2:
        if sys.argv[1] in ["update", "u"]:
            update()
            return
        elif sys.argv[1] in ["help", "h", "--help", "-h"]:
            printHelp()
            return
    elif len(sys.argv) == 3:
        if sys.argv[1] in ["capture", "c"]:
            capture()
            return
        elif sys.argv[1] in ["add", "a"]:
            print("add", sys.argv[2])
            add(sys.argv[2])
            return
        elif sys.argv[1] in ["remove", "r"]:
            remove()
            return

    print("invalid option")
    printHelp()


if __name__ == '__main__':
    if os.getuid() == 0:
        main()
    else:
        print("must be sudo")
